void main(){
  var p1 = Person('DUY', '22');
  var fName = p1.getFirstName();
  var fAge = p1.getAge();
  print('My last name is $fName and i am $fAge years old');
}

class Person{
  String firstName;
  String age;
  Person(this.firstName, this.age);

  String getFirstName(){
    return firstName;
  }

  String getAge(){
    return age;
  }
}
