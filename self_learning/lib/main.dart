import 'package:flutter/material.dart';

void main() {
  runApp(MaterialApp(
    home: demoExcercise()
  ));
}

//DEMO BUTTON
class demoExcercise extends StatelessWidget {
  // const demoExercise({Key? key}) : super(key: key);
  // Stateless widgets: the state of the widget cannot change over time
  // Stateful Widgets: the state of the widget can change over time

  @override
  Widget build(BuildContext context) {
    // return Container();
    return Scaffold(
      appBar: AppBar(
        title: const Text('Duy Tran 519H0011'),
        centerTitle: true,
        backgroundColor: Colors.green[300],
      ),
      body: Center(
        //TEST ICON BUTTON
        child: IconButton(
          onPressed: () {
            print('ICON BUTTON');
          },
          icon: Icon(Icons.facebook),
          iconSize: 100.0,
          color: Colors.blueAccent,
        ),

        //TEST BUTTON ICON
        // child: RaisedButton.icon(
        //   onPressed: () {
        //     print('button icon');
        //   },
        //   icon: Icon(
        //     Icons.facebook,
        //     size: 100.0,
        //       color: Colors.blueAccent,
        //   ),
        //   label: Text('Click Bait!'),
        //   color: Colors.red,
        // ),
        
        // TEST BUTTON
        // child: RaisedButton(
        //   onPressed: () {
        //     print('dak dak dak buh buh');
        //   },
        //   child: Text('CLICK BAIT'),
        //   color: Colors.tealAccent,
        //   hoverColor: Colors.amber,
        //   focusColor: Colors.red,
        // ),
        
        //TEST ICON
        // child: Icon(
        //   Icons.facebook,
        //   size: 100.0,
        //   color: Colors.blueAccent,
        // ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {  },
        child: const Text('Touch'),
        backgroundColor: Colors.cyan,
      ),
    );
  }
}

//DEMO IMAGE API
// class demoExcercise extends StatelessWidget {
//   // const demoExercise({Key? key}) : super(key: key);
//   // Stateless widgets: the state of the widget cannot change over time
//   // Stateful Widgets: the state of the widget can change over time
//
//   @override
//   Widget build(BuildContext context) {
//     // return Container();
//     return Scaffold(
//       appBar: AppBar(
//         title: const Text('Duy Tran 519H0011'),
//         centerTitle: true,
//         backgroundColor: Colors.green[300],
//       ),
//       body: Center(
//         child: Image.network('https://scontent.fsgn4-1.fna.fbcdn.net/v/t1.6435-9/132324656_2616119298678748_424504913525824372_n.jpg?_nc_cat=101&ccb=1-5&_nc_sid=730e14&_nc_ohc=hWntSmFBavkAX8wx51V&_nc_ht=scontent.fsgn4-1.fna&oh=00_AT-edhi18kTDw-6NukstUH_GlzXpZLmo0a2Qxvl320FWCQ&oe=62420129'),//Image.asset('assets/demoImage-1.jpg'),
//       ),
//       floatingActionButton: FloatingActionButton(
//         onPressed: () {  },
//         child: const Text('Touch'),
//         backgroundColor: Colors.cyan,
//       ),
//     );
//   }
// }

